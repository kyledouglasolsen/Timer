﻿using System;
using UnityEngine;

public class TimedStressTest : MonoBehaviour
{
    private static Action emptyCallback = () => { };

    private void Update()
    {
        for (var i = 0; i < 100; ++i)
        {
            Timed.In(1f, emptyCallback);
        }
    }
}