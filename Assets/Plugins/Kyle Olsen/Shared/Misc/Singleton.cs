﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static bool quitting;

    private static T ins;

    public static T Ins => TryCreateInstance();
    public static T DirectIns => ins;

    public static T TryCreateInstance()
    {
        if (ins == null)
        {
            if (quitting)
            {
                return null;
            }

            var singleton = new GameObject
            {
                //hideFlags = HideFlags.HideInHierarchy
            };
            ins = singleton.AddComponent<T>();
            singleton.name = "(singleton) " + typeof(T);

            if (Application.isPlaying)
            {
                DontDestroyOnLoad(singleton);
            }
        }

        return ins;
    }

    protected void SetSingletonInstance(T newInstance)
    {
        ins = newInstance;
    }

    protected virtual void OnApplicationQuit()
    {
        quitting = true;
    }

    private void Awake()
    {
        ins = this as T;
        OnAwake();
    }

    protected abstract void OnAwake();
}