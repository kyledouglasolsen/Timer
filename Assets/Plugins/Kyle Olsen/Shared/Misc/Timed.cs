﻿using System;
using System.Collections.Generic;
using System.Threading;

public class Timed : Singleton<Timed>
{
    private static int frame = int.MinValue, fixedFrame = int.MinValue;
    private static float timeScale = 1f;
    private static readonly List<TimerCallbackDelayed> ActiveDelayedTimers = new List<TimerCallbackDelayed>();
    private static readonly List<TimerCallbackDelayed> ActiveEveryFrameTimers = new List<TimerCallbackDelayed>();
    private static readonly List<TimerCallbackNextFrame> ActiveNextFrameTimers = new List<TimerCallbackNextFrame>();
    private static readonly List<TimerCallbackNextFrame> ActiveNextFixedFrameTimers = new List<TimerCallbackNextFrame>();
    private static readonly List<TimerCallbackDelayed> ActiveFixedFrameTimers = new List<TimerCallbackDelayed>();
    private readonly PooledStack<TimerCallbackDelayed> delayedPool = new PooledStack<TimerCallbackDelayed>(() => new TimerCallbackDelayed(), 8);
    private readonly PooledStack<TimerCallbackNextFrame> nextFramePool = new PooledStack<TimerCallbackNextFrame>(() => new TimerCallbackNextFrame(), 8);

    public static bool UseExternalUpdate { get; private set; }
    public static bool UseExternalFixedUpdate { get; private set; }
    public static bool UseExternalLateUpdate { get; private set; }

    public static void SetTimeScale(float newTimeScale)
    {
        timeScale = newTimeScale;
    }

    public static void SetUseExternalUpdate(bool useExternalUpdate)
    {
        UseExternalUpdate = useExternalUpdate;
    }

    public static void SetUseExternalFixedUpdate(bool useExternalFixedUpdate)
    {
        UseExternalFixedUpdate = useExternalFixedUpdate;
    }

    public static void SetUseExternalLateUpdate(bool useExternalLateUpdate)
    {
        UseExternalLateUpdate = useExternalLateUpdate;
    }

    public static TimerStopper In(float seconds, Action callback)
    {
        return In(seconds, callback, 1);
    }

    public static TimerStopper In(float seconds, Action callback, int repeatCount)
    {
        if (quitting)
        {
            return new TimerStopper();
        }

        var timer = Ins.delayedPool.Pop();
        timer.Setup(seconds, repeatCount, callback);
        ActiveDelayedTimers.Add(timer);
        timer.Start();
        return new TimerStopper(timer);
    }

    public static TimerStopper Every(float seconds, Action callback)
    {
        return Every(seconds, callback, -1);
    }

    public static TimerStopper Every(float seconds, Action callback, int repeatCount)
    {
        return In(seconds, callback, repeatCount);
    }

    public static TimerStopper EveryFrame(Action callback)
    {
        return EveryFrame(callback, -1);
    }

    public static TimerStopper EveryFrame(Action callback, int repeatCount)
    {
        if (quitting)
        {
            return new TimerStopper();
        }

        var timer = Ins.delayedPool.Pop();
        timer.Setup(0f, repeatCount, callback);
        ActiveEveryFrameTimers.Add(timer);
        timer.Start();
        return new TimerStopper(timer);
    }

    public static TimerStopper EveryFixedFrame(Action callback, int repeatCount = -1)
    {
        if (quitting)
        {
            return new TimerStopper();
        }

        var timer = Ins.delayedPool.Pop();
        timer.Setup(0f, repeatCount, callback);
        ActiveFixedFrameTimers.Add(timer);
        timer.Start();
        return new TimerStopper(timer);
    }

    public static void NextFrame(Action callback)
    {
        if (quitting)
        {
            return;
        }

        var timer = Ins.nextFramePool.Pop();
        timer.Setup(callback);
        ActiveNextFrameTimers.Add(timer);
        timer.Start();
    }

    public static void NextFixedFrame(Action callback)
    {
        if (quitting)
        {
            return;
        }

        var timer = Ins.nextFramePool.Pop();
        timer.Setup(callback);
        ActiveNextFixedFrameTimers.Add(timer);
        timer.Start();
    }

    protected override void OnAwake()
    {
    }

    private void Update()
    {
        if (UseExternalUpdate)
        {
            return;
        }

        timeScale = UnityEngine.Time.timeScale;
        ExternalUpdate(UnityEngine.Time.unscaledDeltaTime);
    }

    private void FixedUpdate()
    {
        if (UseExternalFixedUpdate)
        {
            return;
        }

        ExternalFixedUpdate();
    }

    private void LateUpdate()
    {
        if (UseExternalLateUpdate)
        {
            return;
        }

        ExternalLateUpdate();
    }

    public void ExternalUpdate(float delta)
    {
        delta *= timeScale;
        UpdateTimerList(delta, ActiveDelayedTimers, delayedPool);
        UpdateTimerList(delta, ActiveEveryFrameTimers, delayedPool);
        UpdateTimerList(delta, ActiveNextFrameTimers, nextFramePool);
    }

    public void ExternalFixedUpdate()
    {
        UpdateTimerList(0f, ActiveFixedFrameTimers, delayedPool);
        UpdateTimerList(0f, ActiveNextFixedFrameTimers, nextFramePool);
        Interlocked.Increment(ref fixedFrame);
    }

    public void ExternalLateUpdate()
    {
        Interlocked.Increment(ref frame);
    }

    private static void UpdateTimerList<T>(float delta, List<T> list, PooledStack<T> pool) where T : Timer
    {
        for (var i = list.Count - 1; i >= 0; --i)
        {
            var timer = list[i];

            if (timer.Finished)
            {
                pool.Push(timer);
                list.RemoveAt(i);
                continue;
            }

            timer.Step(delta);

            if (timer.Finished)
            {
                pool.Push(timer);
                list.RemoveAt(i);
            }
        }
    }

    internal abstract class Timer
    {
        private int stop;

        public bool Finished { get; private set; }
        public bool Stopped => stop > 0;

        public void Start()
        {
            Finished = false;
            stop = 0;
            OnStart();
        }

        public void Step(float delta = 0f)
        {
            if (Stopped)
            {
                return;
            }

            if (OnStep(delta))
            {
                Finished = true;
                OnFinish();
                Stop();
            }
        }

        protected abstract void OnStart();
        protected abstract bool OnStep(float delta = 0f);
        protected abstract void OnFinish();

        public void Stop()
        {
            Interlocked.Increment(ref stop);
        }
    }

    internal class TimerCallbackDelayed : Timer
    {
        private float currentTime;
        private int remaining;

        public float Delay { get; private set; }
        public int Count { get; private set; }
        public Action Callback { get; private set; }

        public void Setup(float delay, int count, Action callback)
        {
            currentTime = 0f;
            remaining = count;
            Delay = delay;
            Count = count;
            Callback = callback;
        }

        protected override void OnStart()
        {
        }

        protected override bool OnStep(float delta = 0f)
        {
            currentTime += delta;

            if (currentTime >= Delay)
            {
                Callback();
                currentTime -= Delay;

                if (remaining != -1)
                {
                    --remaining;
                }
            }

            return remaining <= 0 && remaining != -1;
        }

        protected override void OnFinish()
        {
        }
    }

    internal class TimerCallbackNextFrame : Timer
    {
        private int frameStarted;

        public Action Callback { get; private set; }

        public void Setup(Action callback)
        {
            Callback = callback;
            frameStarted = frame;
        }

        protected override void OnStart()
        {
        }

        protected override bool OnStep(float delta = 0f)
        {
            return frame != frameStarted;
        }

        protected override void OnFinish()
        {
            Callback();
        }
    }

    public struct TimerStopper
    {
        private bool stopped;
        private Timer timer;

        internal TimerStopper(Timer timer)
        {
            stopped = false;
            this.timer = timer;
        }

        public void Stop()
        {
            if (stopped)
            {
                return;
            }

            timer?.Stop();
            stopped = true;
        }
    }
}