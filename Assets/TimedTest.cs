﻿using UnityEngine;

public class TimedTest : MonoBehaviour
{
    private void OnEnable()
    {
        TestIn();
        //TestEvery();
        //TestEveryFrame();
        //TestEveryFixedFrame();
        //TestNextFrame();
        //TestNextFixedFrame();

        //TestInWithTimeScale();
        //TestTimerStop();
    }

    private void TestIn()
    {
        Debug.Log($"Started {Time.time}");
        Timed.In(1f, () => { Debug.Log($"Finished {Time.time}"); });
    }

    private void TestEvery()
    {
        Debug.Log($"Started {Time.time}");
        Timed.Every(1f, () => { Debug.Log($"Update {Time.time}"); });
    }

    private void TestEveryFrame()
    {
        Debug.Log($"Started {Time.frameCount}");
        Timed.EveryFrame(() => { Debug.Log($"Update {Time.frameCount}"); });
    }

    private void TestEveryFixedFrame()
    {
        Debug.Log($"Started {Time.frameCount}");
        Timed.EveryFixedFrame(() => { Debug.Log($"FixedUpdate {Time.frameCount}"); });
    }

    private void TestNextFrame()
    {
        Debug.Log($"Started {Time.frameCount}");
        Timed.NextFrame(() => { Debug.Log($"Finished {Time.frameCount}"); });
    }

    private void TestNextFixedFrame()
    {
        Debug.Log($"Started {Time.frameCount}");
        Timed.NextFixedFrame(() => { Debug.Log($"Finished {Time.frameCount}"); });
    }

    private void TestInWithTimeScale()
    {
        Time.timeScale = 2f;

        Debug.Log($"Started {Time.unscaledTime}");
        Timed.In(5f, () => { Debug.Log($"Finished {Time.unscaledTime}"); });
    }

    private void TestTimerStop()
    {
        Debug.Log($"Started {Time.time}");
        var stop = Timed.In(5f, () => { Debug.Log($"Finished {Time.time}"); });
        stop.Stop();
    }
}